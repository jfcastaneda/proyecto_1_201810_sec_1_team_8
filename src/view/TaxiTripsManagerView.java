package view;

import java.text.ParseException;
import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.List;
import model.logic.Chicago;
import model.vo.Compania;
import model.vo.InfoCompaniaTopX;
import model.vo.InfoCompanias;
import model.vo.InfoTaxi;
import model.vo.InfoZona;
import model.vo.InfoZonaB4;
import model.vo.Servicio;
import model.vo.Taxi;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = Chicago.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = Chicago.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = Chicago.DIRECCION_LARGE_JSON;
					break;
				}

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.cargarSistema(linkJson);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

				//1A	
			case 2:

				long a = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long b = System.nanoTime();

				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq1A = sc.next();

				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq1A = sc.next();

				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq1A = sc.next();

				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq1A = sc.next();

				try
				{
					long inicio = Chicago.FORMAT.parse(fechaInicialReq1A + "T" + horaInicialReq1A).getTime();

					long fino = Chicago.FORMAT.parse(fechaFinalReq1A + "T" + horaFinalReq1A).getTime();

					IQueue<Servicio> a1 = Controller.darServiciosEnRango(inicio, fino);

					Servicio s1;
					int i = 1;

					while(!a1.isEmpty())
					{
						s1 = a1.dequeue();
						System.out.println(i++ + ". Servicio con id: " + s1.id() + "\nFecha inicio (en milisegundos): "
								+ s1.inicio() + "\nFecha fin (en milisegundos): " + s1.fin());
					}
				}
				catch(ParseException u)
				{
					System.out.println(u);
				}

				long c = System.nanoTime();
				long d = (c- b)/(1000000);

				long e = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("-----------------------------------------------------");
				System.out.println("Tiempo en cargar: " + d + " milisegundos \nMemoria utilizada:  "+ ((e - a)/1000000.0) + " MB");
				System.out.println("\n");

				//Se obtiene la queue dada el rango
				//TODO 
				//Recorra la cola y muestre cada servicio en ella
				break;

			case 3: //2A
				sc.nextLine();
				System.out.println("Ingrese el nombre de la compa��a");
				String companyReq2A = sc.nextLine();
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq2A = sc.next();
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq2A = sc.next();
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq2A = sc.next();
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq2A = sc.next();
				
				try
				{
					long inicio = Chicago.FORMAT.parse(fechaInicialReq2A + "T" + horaInicialReq2A).getTime();
					long fino = Chicago.FORMAT.parse(fechaFinalReq2A + "T" + horaFinalReq2A).getTime();
					System.out.println("El taxi fue: " + Controller.darTaxiConMasServiciosEnCompaniaYRango(companyReq2A, inicio, fino));
				}
				catch(ParseException u)
				{
					System.out.println(u);
				}

				break;

			case 4: //3A

				System.out.println("Ingrese el id del taxi");
				String idTaxiReq3A = sc.next();
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq3A = sc.next();
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq3A = sc.next();
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq3A = sc.next();
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq3A = sc.next();

				try
				{
					long inicio = Chicago.FORMAT.parse(fechaInicialReq3A + "T" + horaInicialReq3A).getTime();
					long fino = Chicago.FORMAT.parse(fechaFinalReq3A + "T" + horaFinalReq3A).getTime();
					InfoTaxi infoTaxi = Controller.darInformacionTaxiEnRango(idTaxiReq3A, inicio, fino);
					System.out.println("Coma��a taxi: " + infoTaxi.compania() + "\nDinero ganado: " + infoTaxi.ganancia()
					+ "\nCantidad servicios prestados: " + infoTaxi.servicios() + "\nDistancia recorrida: " + infoTaxi.distancia()
					+ "\nTiempo total de servicios: " + infoTaxi.tiempo());
				}
				catch(Exception u)
				{
					System.out.println(u);
				}

				break;

			case 5: //4A

//				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
//				String fechaReq4A = sc.next();
//				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
//				String horaInicialReq4A = sc.next();
//				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
//				String horaFinalReq4A = sc.next();
//
//				//List<RangoDistancia> listaReq4A = Controller.darListaRangosDistancia(fechaReq4A, horaInicialReq4A, horaFinalReq4A);
//
//				//TODO
//				//Recorra la lista y por cada VORangoDistancia muestre los servicios

				break;

			case 6: //1B
				System.out.println("\n-----------------------------------------------------");
				long a2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long b2 = System.nanoTime();

				int i2 = 1;
				InfoCompanias infoCompanias = Controller.darCompaniasTaxisInscritos();
				System.out.println("Total compa��as con al menos un taxi inscrito: " + infoCompanias.companiasTaxi()
				+ "\nTotal taxis que prestan un servicio para al menos una compa��a: " + infoCompanias.taxisCompanias()
				+ "\nCompa��as a las cuales aparecen vinculados los taxis:");
				Iterator<Compania> iterB1 = infoCompanias.companias().iterator();
				while(iterB1.hasNext())
				{
					System.out.println(i2++ + ". " + iterB1.next().toString());
				}

				long c2 = System.nanoTime();
				long d2 = (c2- b2)/(1000000);
				long e2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

				System.out.println("-----------------------------------------------------");
				System.out.println("Tiempo en cargar: " + d2 + " milisegundos \nMemoria utilizada:  "+ ((e2 - a2)/1000000.0) + " MB");
				System.out.println("\n");

				break;

			case 7: //2B
				sc.nextLine();
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq2B = sc.next();
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq2B = sc.next();
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq2B = sc.next();
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq2B = sc.next();
				System.out.println("Ingrese el nobre de la compa�ia");
				String compania2B=sc.nextLine();

				try
				{
					long inicio = Chicago.FORMAT.parse(fechaInicialReq2B + "T" + horaInicialReq2B).getTime();
					long fino = Chicago.FORMAT.parse(fechaFinalReq2B + "T" + horaFinalReq2B).getTime();
					System.out.println("El taxi de mayor facturaci�n es: " + Controller.darTaxiMayorFacturacion(compania2B, inicio, fino));
				}
				catch(Exception u)
				{
					System.out.println(u);
				}

				break;

			case 8: //3B
				System.out.println("Ingrese el id de la zona");
				String zona3B=sc.next();
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq3B = sc.next();
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq3B = sc.next();
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq3B = sc.next();
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq3B = sc.next();

				try
				{
					long inicio = Chicago.FORMAT.parse(fechaInicialReq3B + "T" + horaInicialReq3B).getTime();
					long fino = Chicago.FORMAT.parse(fechaFinalReq3B + "T" + horaFinalReq3B).getTime();
					InfoZona infoZona = Controller.darServiciosZonaValorTotal(zona3B, inicio, fino);
					System.out.println("Servicios que se recogieron en esa zona: " + infoZona.iniciaronCantidad()
					+ "\nValor pagado por los usuarios: " + infoZona.iniciaronGanancia() 
					+ "\nServicios que terminaron en esa zona: " + infoZona.terminaronCantidad()
					+ "\nValor pagado por los usuarios: " + infoZona.terminaronGanancia()
					+ "\nServicios que empezaron y terminaron en esa zona: " + infoZona.ambasCantidad()
					+ "\nValor pagado por los usuarios: " + infoZona.ambasGanancia());

				}
				catch(Exception u)
				{
					System.out.println(u);
				}

				break;

			case 9: //4B
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq4B = sc.next();
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq4B = sc.next();
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq4B = sc.next();
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq4B = sc.next();

				try
				{
					long inicio = Chicago.FORMAT.parse(fechaInicialReq4B + "T" + horaInicialReq4B).getTime();
					long fino = Chicago.FORMAT.parse(fechaFinalReq4B + "T" + horaFinalReq4B).getTime();
					List<InfoZonaB4> infoZona = Controller.darZonasServicios(inicio, fino);
					Iterator<InfoZonaB4> iter = infoZona.iterator();
					InfoZonaB4 zona;
					int i = 1;
					while(iter.hasNext())
					{
						zona = iter.next();
						System.out.println(i++ + ". Id de la zona: " + zona.id() + "\nCantidad de servicios iniciados en esa zona: "
								+ zona.cantidadIniciados());
					}
				}
				catch(Exception u)
				{
					System.out.println(u);
				}

				break;

			case 10: //2C
				System.out.println("Ingrese el n�mero n de compa�ias");
				int n2C=sc.nextInt();
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq2C = sc.next();
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq2C = sc.next();
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq2C = sc.next();
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq2C = sc.next();

				try
				{
					long inicio = Chicago.FORMAT.parse(fechaInicialReq2C + "T" + horaInicialReq2C).getTime();
					long fino = Chicago.FORMAT.parse(fechaFinalReq2C + "T" + horaFinalReq2C).getTime();
					List<InfoCompaniaTopX> info = Controller.companiasMasServicios(n2C, inicio, fino);
					Iterator<InfoCompaniaTopX> iter = info.iterator();
					InfoCompaniaTopX compania;
					int i = 1;
					while(iter.hasNext())
					{
						compania = iter.next();
						System.out.println(i++ + ". Nombre de la compa��a: " + compania.nombre() 
						+ "\nCantidad de servicios de la compa��a: " + compania.cantidadServicios());
					}
				}
				catch(Exception u)
				{
					System.out.println(u);
				}

				break;

			case 11: //3C
				System.out.println("\n-----------------------------------------------------");

				long a1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long b1 = System.nanoTime();

				Iterator<Taxi> iter = Controller.taxisMasRentables().iterator();
				Taxi taxi;
				int i1 = 1;
				System.out.println();
				while(iter.hasNext())
				{
					taxi = iter.next();
					System.out.println((i1++) + ". Compa��a: " + taxi.compania().nombre() + "\nTaxi m�s rentable: "
							+ taxi.id());
				}		

				long c1 = System.nanoTime();
				long d1 = (c1- b1)/(1000000);
				long e1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

				System.out.println("-----------------------------------------------------");
				System.out.println("Tiempo en cargar: " + d1 + " milisegundos \nMemoria utilizada:  "+ ((e1 - a1)/1000000.0) + " MB");
				System.out.println("\n");

				break;

			case 12: //4C

				System.out.println("Ingrese el id del taxi");
				String idTaxi4C=sc.next();
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaReq4C = sc.next();
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq4C = sc.next();
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq4C = sc.next();

				try
				{
					long inicio = Chicago.FORMAT.parse(fechaReq4C + "T" + horaInicialReq4C).getTime();
					long fino = Chicago.FORMAT.parse(fechaReq4C + "T" + horaFinalReq4C).getTime();
					IStack <Servicio> resp4C=Controller.darServicioResumen(idTaxi4C, inicio, fino);
					Servicio servicio;
					int i = 1;
					while(!resp4C.isEmpty())
					{
						servicio = resp4C.pop();
						System.out.println(i++ + ". Id del servicio: " + servicio.id() 
						+ "\nHora inicial: " + servicio.inicio()
						+ "\nHora final: " + servicio.fin()
						+ "\nDuracion: " + servicio.tiempo()
						+ "\nGanancia: " + servicio.ganancia()
						+ "\nDistancia: " + servicio.distancia());
					}
				}
				catch(Exception u)
				{
					System.out.println(u);
				}

				break;

			case 13: //salir
				fin=true;
				sc.close();
				break;

			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("Cargar data (1C):");
		System.out.println("1. Cargar toda la informacion dada una fuente de datos (small,medium, large).");

		System.out.println("\nParte A:\n");
		System.out.println("2.Obtenga una lista con todos los servicios de taxi ordenados cronologicamente por su fecha/hora inicial, \n"
				+ " que se prestaron en un periodo de tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta. (1A) ");
		System.out.println("3.Dada una compania y un rango de fechas y horas, obtenga  el taxi que mas servicios inicio en dicho rango. (2A)");
		System.out.println("4.Consulte la compania, dinero total obtenido, servicios prestados, distancia recorrida y tiempo total de servicios de un taxi dados su id y un rango de fechas y horas. (3A)");
		System.out.println("5.Dada una fecha y un rango de horas, obtenga una lista de rangos de distancia en donde cada pocision contiene los servicios de taxis cuya distancia recorrida pertence al rango de distancia. (4A)\n");

		System.out.println("Parte B: \n");
		System.out.println("6. Obtenga el numero de companias que tienen al menos un taxi inscrito y el numero total de taxis que trabajan para al menos una compania. \n"
				+ "Luego, genera una lista (en orden alfabetico) de las companias a las que estan inscritos los servicios de taxi. Para cada una, indique su nombre y el numero de taxis que tiene registrados. (1B)");
		System.out.println("7. Dada una compania, una fecha/hora inicial y una fecha/hora final, buscar el taxi de la compania que mayor facturacion gener� en el tiempo dado. (2B)");
		System.out.println("8. Dada una zona de la ciudad, una fecha/hora inicial y una fecha/hora final, dar la siguiente informacion: (3B) \n"
				+ "   -Numero de servicios que iniciaron en la zona dada y terminaron en otra zona, junto con el valor total pagado por los usuarios. \n"
				+ "   -Numero de servicios que iniciaron en otra zona y terminaron en la zona dada, junto con el valor total pagado por los usuarios. \n"
				+ "   -Numero de servicios que iniciaron y terminaron en la zona dada, junto con el valor total pagado por los usuarios.");
		System.out.println("9. Dado un rango de fechas, obtener la lista de todas las zonas, ordenadas por su identificador. Para cada zona, dar la lista de fechas dentro del rango (ordenadas cronol�gicamente) \n "
				+ "y para cada fecha, dar el numero de servicios que se realizaron en dicha fecha. (4B)");


		System.out.println("\nParte C: \n");
		System.out.println("10. Dado un numero n, una fecha/hora inicial y una fecha/hora final, mostrar las n companias que mas servicios iniciaron dentro del rango. La lista debe estar ordenada descendentemente \n por el numero de servicios. Para cada compania, dar el nombre y el numero de servicios (2C)");
		System.out.println("11. Para cada compania, dar el taxi mas rentable. La rentabilidad es dada por la relacion entre el dinero ganado y la distancia recorrida. (3C)");
		System.out.println("12. Dado un taxi, dar el servicio resumen, resultado de haber comprimido su informacion segun el enunciado. (4C) ");
		System.out.println("13. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}
