package controller;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.List;
import model.logic.Chicago;
import model.vo.Compania;
import model.vo.InfoCompaniaTopX;
import model.vo.InfoCompanias;
import model.vo.InfoTaxi;
import model.vo.InfoZona;
import model.vo.InfoZonaB4;
import model.vo.Servicio;
import model.vo.Taxi;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static Chicago manager = new Chicago();

	//1C
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.c1(direccionJson);
	}
	//A1
	public static IQueue<Servicio> darServiciosEnRango(long inicio, long fin)
	{
		return manager.a1(inicio, fin);
	}

	//2A
	public static String darTaxiConMasServiciosEnCompaniaYRango(String company, long inicio, long fin)
	{
		return manager.a2(company, inicio, fin);
	}

	//3A
	public static InfoTaxi darInformacionTaxiEnRango(String id, long inicio, long fin) throws Exception
	{
		return manager.a3(id, inicio, fin);
	}

	//	//4A
	//	public static List<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	//	{
	//		return manager.darListaRangosDistancia(fecha, horaInicial, horaFinal);
	//	}
	//	
	//1B
	public static InfoCompanias darCompaniasTaxisInscritos()
	{
		return manager.b1();
	}

	//2B
	public static String darTaxiMayorFacturacion(String nomCompania, long inicio, long fin)
	{
		return manager.b2(nomCompania, inicio, fin);
	}

	//3B
	public static InfoZona darServiciosZonaValorTotal(String idZona, long inicio, long fin) throws Exception
	{
		return manager.b3(idZona, inicio, fin);
	}

	//4B
	public static List<InfoZonaB4> darZonasServicios (long inicio, long fin)
	{
		return manager.b4(inicio, fin);
	}

	//2C
	public static List<InfoCompaniaTopX> companiasMasServicios(int n, long inicio, long fin)
	{
		return manager.c2(n, inicio, fin);
	}

	//3C
	public static List<Taxi> taxisMasRentables()
	{
		return manager.c3();
	}

	//4C
	public static IStack <Servicio> darServicioResumen(String taxiId, long inicio, long fin) throws Exception
	{
		return manager.c4(taxiId, inicio, fin);
	}
}
