package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.List;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.utils.ComparatorCompaniaNombre;
import model.utils.ComparatorInfoCompaniaTopX;
import model.utils.ComparatorServicioCompaniaNombre;
import model.utils.ComparatorServicioDistancia;
import model.utils.ComparatorServicioFecha;
import model.utils.ComparatorServicioTaxiId;
import model.utils.ComparatorServicioZonaIdYFecha;
import model.utils.ComparatorTaxiId;
import model.utils.ComparatorZona;
import model.vo.Compania;
import model.vo.InfoCompaniaTopX;
import model.vo.InfoCompanias;
import model.vo.InfoTaxi;
import model.vo.InfoZona;
import model.vo.InfoZonaB4;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.Zona;

/**
 * Todos los taxis son ordenados por id.
 * Todos los servicios son ordenados por tiempo en que se tomaron.
 * Todas las companias son ordenadas por alfabeto.
 */
public class Chicago implements ITaxiTripsManager
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";

	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";

	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	public static final SimpleDateFormat FORMAT = new SimpleDateFormat("YYYY-MM-DD'T'HH:mm:ss'.'000");

	public final static ComparatorTaxiId COMPARATOR_TAXI_ID = new ComparatorTaxiId();

	public final static ComparatorServicioFecha COMPARATOR_SERVICIO_FECHA = new ComparatorServicioFecha();

	public final static ComparatorServicioCompaniaNombre COMPARATOR_SERVICIO_COMPANIA_NOMBRE = new ComparatorServicioCompaniaNombre();

	public final static ComparatorServicioZonaIdYFecha COMPARATOR_SERVICIO_ZONA_ID_Y_FECHA = new ComparatorServicioZonaIdYFecha();

	public final static ComparatorCompaniaNombre COMPARATOR_COMPANIA_NOMBRE = new ComparatorCompaniaNombre();

	public final static ComparatorServicioTaxiId COMPARATOR_SERVICIO_TAXI_ID = new ComparatorServicioTaxiId();

	public final static ComparatorServicioDistancia COMPARATOR_SERVICIO_DISTANCIA = new ComparatorServicioDistancia();

	public final static ComparatorInfoCompaniaTopX COMPARATOR_INFO_COMPANIA_TOP_X = new ComparatorInfoCompaniaTopX();

	public final static ComparatorZona COMPARATOR_ZONA = new ComparatorZona();

	private List<Taxi> taxis;

	private List<Taxi> taxisMasRentables;

	private List<Servicio> serviciosPorCompaniaNombre;

	private List<Servicio> serviciosPorFecha;

	private List<Servicio> serviciosPorZonaIdYFecha;

	private List<Compania> companias;

	private List<Compania> companiasTaxi;

	private List<Zona> zonasPorId;

	private InfoCompanias infoCompanias;

	public Chicago()
	{
		taxis = new List<Taxi>();
		serviciosPorCompaniaNombre = new List<Servicio>();
		serviciosPorFecha = new List<Servicio>();
		serviciosPorZonaIdYFecha = new List<Servicio>();
		companias = new List<Compania>();
		companiasTaxi = new List<Compania>();
		zonasPorId = new List<Zona>();
	}

	public IQueue<Servicio> a1(long inicio, long fin)
	{
		IQueue<Servicio> queue = new Queue<Servicio>();
		Iterator<Servicio> iter = serviciosPorFecha.iterator();
		Servicio servicio;	

		while(iter.hasNext())
		{
			servicio = iter.next();

			if(servicio.inicio() >= inicio && servicio.fin() <= fin)
			{
				queue.enqueue(servicio);
			}
			else if(servicio.inicio() > fin)
			{
				break;
			}
		}

		return queue;
	}

	public String a2(String compania, long inicio, long fin)
	{
		Compania c = null;
		Iterator<Compania> iterCompanias = companiasTaxi.iterator();
		while(iterCompanias.hasNext())
		{
			c = iterCompanias.next();
			if(c.nombre().equals(compania))
			{
				break;
			}
		}
		if(c == null)
		{
			return "La compa��a buscada no existe.";
		}

		Servicio s;
		Taxi mayor = null;
		int numServicios = 0;
		int numServiciosTaxi = 0;
		Taxi actual = null;
		Iterator<Servicio> iterServicios = c.serviciosPorTaxiId().iterator();
		while(iterServicios.hasNext())
		{
			s = (Servicio)iterServicios.next();
			if(s.inicio() >= inicio && s.fin() <= fin)
			{
				if(actual == null || COMPARATOR_TAXI_ID.compare(actual, s.taxi()) != 0)
				{
					actual = s.taxi();
					numServiciosTaxi = 1;
				}
				else
				{
					numServiciosTaxi++;
				}
				if(numServiciosTaxi > numServicios)
				{
					mayor = actual;
					numServicios = numServiciosTaxi;
				}
			}
		}
		if(mayor == null)
		{
			return "La compa��a no tiene servicios de taxi";
		}
		return mayor.toString();
	}

	public InfoTaxi a3(String id, long inicio, long fin) throws Exception
	{
		Servicio s;
		Taxi t = null;
		double ganancia = 0;
		int cantidadServicios = 0;
		double distancia = 0;
		long tiempo = 0;
		Iterator<Taxi> iterTaxis = taxis.iterator();
		while(iterTaxis.hasNext())
		{
			t = iterTaxis.next();
			if(t.id().equals(id))
			{
				break;
			}
		}
		if(t == null)
		{
			throw new Exception("No existe ning�n taxi con tal id.");
		}
		else if(t.serviciosPorFecha().isEmpty())
		{
			throw new Exception("El taxi no ha realizado ning�n servicio");
		}

		Iterator<Servicio> iterServicios = t.serviciosPorFecha().iterator();
		while(iterServicios.hasNext())
		{
			s = iterServicios.next();
			if(s.inicio() >= inicio && s.fin() <= fin)
			{
				ganancia += s.ganancia();
				cantidadServicios++;
				distancia += s.distancia();
				tiempo += s.tiempo();
			}
			else if(s.inicio() > fin)
			{
				break;
			}
		}

		return new InfoTaxi(t.compania().toString(), ganancia, cantidadServicios, distancia, tiempo);
	}

	public Servicio[] a4(long inicio, long fin)
	{
		//		List<List<Servicio>> alv = new List<List<Servicio>>();
		//		serviciosCompaniaPorDistancia.sort(COMPARATOR_SERVICIO_DISTANCIA);
		//		// TODO: Ver si la ordeno desde un principio.
		//		Iterator<Servicio> iter = serviciosCompaniaPorDistancia.iterator();
		//
		//		while(iter.hasNext())
		//		{
		//			if()
		//			{
		//
		//			}
		//		}
		return null;
	}

	public InfoCompanias b1()
	{
		return infoCompanias;
	}

	public String b2(String compania, long inicio, long fin)
	{
		Compania c = null;
		Iterator<Compania> iterCompanias = companiasTaxi.iterator();
		while(iterCompanias.hasNext())
		{
			c = (Compania)iterCompanias.next();
			if(c.nombre().equals(compania))
			{
				break;
			}
		}
		if(c == null)
		{
			return "La compa��a buscada no existe.";
		}

		Servicio s;
		Taxi mayor = null;
		double facturacion = 0;
		double facturacionTaxi = 0;
		Taxi actual = null;
		Iterator<Servicio>iter = c.serviciosPorTaxiId().iterator();
		while(iter.hasNext())
		{
			s = (Servicio) iter.next();
			if(s.inicio() >= inicio && s.fin() <= fin)
			{
				if(actual == null || COMPARATOR_TAXI_ID.compare(actual, s.taxi()) != 0)
				{
					actual = s.taxi();
					facturacionTaxi = s.ganancia();
				}
				else
				{
					facturacionTaxi += s.ganancia();
				}
				if(facturacionTaxi > facturacion)
				{
					mayor = actual;
					facturacion = facturacionTaxi;
				}
			}
		}
		if(mayor == null)
		{
			return "La compa��a no tiene servicios de taxi";
		}
		return mayor.toString();
	}

	public InfoZona b3(String id, long inicio, long fin) throws Exception
	{
		Zona z = null;
		Iterator<Zona> iterZonas = zonasPorId.iterator();
		int cantidadInicio = 0, cantidadFin = 0, cantidadAmbos = 0; 
		double gananciaInicio = 0, gananciaFin = 0, gananciaAmbos = 0;

		while(iterZonas.hasNext())
		{
			z = (Zona)iterZonas.next();
			if(z.id().equals(id))
			{
				break;
			}
		}
		if(z == null)
		{
			throw new Exception("No existe una zona con tal Id.");
		}

		Iterator<Servicio> iterServicios = z.serviciosPorFecha().iterator();
		Servicio s = null;
		while(iterServicios.hasNext())
		{
			s = (Servicio) iterServicios.next();
			if(s.inicio() >= inicio || s.fin() <= fin)
			{
				if(s.salida() != null && s.llegada() != null && 
						COMPARATOR_ZONA.compare(s.salida(), s.llegada()) == 0)
				{
					cantidadAmbos ++;
					gananciaAmbos += s.ganancia();
				}
				else if(s.salida() != null && s.salida().id().equals(id))
				{
					cantidadInicio ++;
					gananciaInicio += s.ganancia();
				}
				else if(s.llegada() != null && s.llegada().id().equals(id))
				{
					cantidadFin ++;
					gananciaFin += s.ganancia();
				}
			}
			else if(s.inicio() > fin)
			{
				break;
			}
		}
		if(s == null)
		{
			throw new Exception("No existen servicios que involucren esa zona.");
		}

		return new InfoZona(cantidadInicio, gananciaInicio, cantidadFin, gananciaFin, 
				cantidadAmbos, gananciaAmbos);
	}

	public List<InfoZonaB4> b4(long inicio, long fin)
	{
		Iterator<Servicio> iter = serviciosPorZonaIdYFecha.iterator();
		List<InfoZonaB4> infoZonas = new List<InfoZonaB4>();
		Servicio s = null;
		Zona z = null;
		int cantidadServicios = 0;

		while(iter.hasNext())
		{
			s = iter.next();
			if(s.inicio() >= inicio && s.fin() <= fin)
			{
				if(z == null || !z.id().equals(s.salida().id()))
				{
					if(z != null)
					{
						infoZonas.agregarAlFinal(new InfoZonaB4(z.id(), cantidadServicios));
					}
					z = s.salida();
					cantidadServicios = 1;
				}
				else
				{
					cantidadServicios ++;
				}
			}
		}

		return infoZonas;
	}

	@SuppressWarnings("unused")
	public boolean c1(String direccionJson)
	{
		JsonParser parser = new JsonParser();
		Servicio servicio;
		Taxi taxi, taxi2;
		Compania compania, compania2;
		Zona salida, llegada, zona2;
		long inicio, fin;
		int taxisConCompania = 0;
		boolean existe, existe2;
		Iterator<Taxi> iterTaxis;
		Iterator<Zona> iterZonas;
		Iterator<Compania> iterCompanias;

		try
		{
			JsonArray arr= (JsonArray) parser.parse(new FileReader(direccionJson));

			for(int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				taxi = null;
				compania = null;
				salida = null;
				llegada = null;
				servicio = null;
				existe = false;

				if(obj.get("company") != null)
				{
					compania = new Compania(obj.get("company").getAsString());
				}
				if(obj.get("taxi_id") != null)
				{
					taxi = new Taxi(
							obj.get("taxi_id") != null ? obj.get("taxi_id").getAsString() : null,
									compania
							);
				}

				if(obj.get("pickup_community_area") != null)
				{
					salida = new Zona(obj.get("pickup_community_area").getAsString());
				}

				if(obj.get("dropoff_community_area") != null)
				{
					llegada = new Zona(obj.get("dropoff_community_area").getAsString());
				}

				if(obj.get("trip_id") != null)
				{
					servicio = new Servicio(
							obj.get("trip_id").getAsString(),
							inicio = obj.get("trip_start_timestamp") != null ? FORMAT.parse(obj.get("trip_start_timestamp").getAsString()).getTime() : 0,
									fin = obj.get("trip_end_timestamp") != null ? FORMAT.parse(obj.get("trip_end_timestamp").getAsString()).getTime() : Long.MAX_VALUE,
											(obj.get("tips") != null ? obj.get("tips").getAsDouble() : 0) +
											(obj.get("tolls") != null ? obj.get("tolls").getAsDouble() : 0) +
											(obj.get("extras") != null ? obj.get("extras").getAsDouble() : 0) +
											(obj.get("fare") != null ? obj.get("fare").getAsDouble() : 0),
											obj.get("trip_miles") != null ? obj.get("trip_miles").getAsDouble() : 0,
													fin - inicio,
													salida,
													llegada,
													taxi,
													compania
							);
				}

				if(servicio == null || !serviciosPorCompaniaNombre.contains(servicio))
				{
					if(servicio != null) 
					{
						serviciosPorFecha.agregarAlFinal(servicio);
					}
					if(taxi != null)
					{
						existe = false;
						if(servicio != null) 
						{
							taxi.serviciosPorFecha().agregarAlFinal(servicio);
						}
						iterTaxis = taxis.iterator();
						while(iterTaxis.hasNext() && !existe)
						{
							taxi2 = iterTaxis.next();
							if(COMPARATOR_TAXI_ID.compare(taxi, taxi2) == 0)
							{	
								existe = true;
								if(servicio != null)
								{
									taxi2.serviciosPorFecha().agregarAlFinal(servicio);
								}
							}
						}
						if(!existe)
						{
							taxis.agregarAlFinal(taxi);
							if(compania != null)
							{
								taxisConCompania++;
							}
						}
					}
					if(compania != null)
					{
						existe = false;
						if(servicio != null)
						{
							compania.serviciosPorTaxiId().agregarAlFinal(servicio);
							serviciosPorCompaniaNombre.agregarAlFinal(servicio);
						}
						compania2 = null;
						iterCompanias = companias.iterator();
						while(iterCompanias.hasNext())
						{
							compania2 = iterCompanias.next();
							if(COMPARATOR_COMPANIA_NOMBRE.compare(compania, compania2) == 0)
							{
								if(servicio != null)
								{
									compania2.serviciosPorTaxiId().agregarAlFinal(servicio);
								}
								existe = true;
								break;
							}
						}
						if(!existe)
						{
							companias.agregarAlFinal(compania);
							if(taxi != null)
							{
								companiasTaxi.agregarAlFinal(compania);
							}
						}
					}
					if(salida != null || llegada != null)
					{
						existe = false;
						existe2 = false;
						iterZonas =	 zonasPorId.iterator();
						while(iterZonas.hasNext() && !existe && !existe2)
						{
							zona2 = iterZonas.next();
							if(salida != null && COMPARATOR_ZONA.compare(salida, zona2) == 0)
							{
								existe = true;
								salida = zona2;
							}
							if(llegada != null && COMPARATOR_ZONA.compare(llegada, zona2) == 0)
							{
								existe2 = true;
								llegada = zona2;
							}
						}
						if(servicio != null && (existe || existe2))
						{
							servicio.cambiarSalida(salida);
							servicio.cambiarLlegada(llegada);	
						}
						if(salida != null && llegada != null)
						{
							if(servicio != null)
							{
								salida.serviciosPorFecha().agregarAlFinal(servicio);
								serviciosPorZonaIdYFecha.agregarAlFinal(servicio);
							}
							if(!existe)	
							{
								zonasPorId.agregarAlFinal(salida);
							}
							if(COMPARATOR_ZONA.compare(salida, llegada) != 0)
							{
								if(servicio != null)
								{
									llegada.serviciosPorFecha().agregarAlFinal(servicio);
								}	
								if(!existe2)
								{
									zonasPorId.agregarAlFinal(llegada);
								}
							}
						}
						else if(salida != null && llegada == null)
						{
							if(servicio != null)
							{
								salida.serviciosPorFecha().agregarAlFinal(servicio);
								serviciosPorZonaIdYFecha.agregarAlFinal(servicio);
							}	
							if(!existe)	
							{
								zonasPorId.agregarAlFinal(salida);
							}
						}
						else if(salida == null && llegada != null)
						{
							if(servicio != null)
							{
								llegada.serviciosPorFecha().agregarAlFinal(servicio);
							}
							if(!existe2)	
							{
								zonasPorId.agregarAlFinal(llegada);
							}
						}
					}
				}
			}
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();
			return false;
		}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();
			return false;
		}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();
			return false;
		}
		catch(ParseException e4)
		{
			e4.printStackTrace();
			return false;
		}

		taxis.sort(COMPARATOR_TAXI_ID);
		for(Taxi t: taxis)
		{
			t.serviciosPorFecha().sort(COMPARATOR_SERVICIO_FECHA);
		}
		serviciosPorCompaniaNombre.sort(COMPARATOR_SERVICIO_COMPANIA_NOMBRE);
		serviciosPorFecha.sort(COMPARATOR_SERVICIO_FECHA);
		serviciosPorZonaIdYFecha.sort(COMPARATOR_SERVICIO_ZONA_ID_Y_FECHA);
		companias.sort(COMPARATOR_COMPANIA_NOMBRE);
		for(Compania c: companias)
		{
			c.serviciosPorTaxiId().sort(COMPARATOR_SERVICIO_TAXI_ID);
		}
		companiasTaxi.sort(COMPARATOR_COMPANIA_NOMBRE);
		zonasPorId.sort(COMPARATOR_ZONA);
		for(Zona z: zonasPorId)
		{
			z.serviciosPorFecha().sort(COMPARATOR_SERVICIO_FECHA);
		}
		infoCompanias = new InfoCompanias(companiasTaxi.size(), taxisConCompania, companiasTaxi);

		double gananciaTotal = 0, distanciaTotal = 0, rentabilidad = 0;
		Iterator<Servicio> iterServicios;
		iterCompanias = companiasTaxi.iterator();
		taxisMasRentables = new List<Taxi>();
		while(iterCompanias.hasNext())
		{
			compania = iterCompanias.next();
			iterServicios = compania.serviciosPorTaxiId().iterator();
			taxi2 = null;
			taxi = null;
			while(iterServicios.hasNext())
			{
				servicio = iterServicios.next();
				if(taxi == null || !taxi.id().equals(servicio.taxi().id()))
				{
					if(gananciaTotal/(distanciaTotal != 0 ? distanciaTotal : 1) >= rentabilidad)
					{
						rentabilidad = gananciaTotal/(distanciaTotal != 0 ? distanciaTotal : 1);
						gananciaTotal = 0;
						distanciaTotal = 0;
						taxi2 = taxi;
					}
					taxi = servicio.taxi();
				}
				else
				{
					gananciaTotal += servicio.ganancia();
					distanciaTotal += servicio.distancia();
				}
			}
			if(taxi2 != null)
			{
				taxisMasRentables.agregarAlFinal(taxi2);
			}
		}

		return true;
	}

	public List<InfoCompaniaTopX> c2(int n, long inicio, long fin)
	{
		List<InfoCompaniaTopX> infoCompanias = new List<InfoCompaniaTopX>();
		Iterator<Servicio> iterServicios = serviciosPorCompaniaNombre.iterator();
		Servicio s = null;
		int numServicios = 0;
		Compania actual = null;
		while(iterServicios.hasNext())
		{
			s = iterServicios.next();
			if(s.inicio() >= inicio && s.fin() <= fin)
			{
				if(actual == null || COMPARATOR_COMPANIA_NOMBRE.compare(actual, s.compania()) != 0)
				{
					if(actual != null)
					{
						infoCompanias.agregarAlFinal(new InfoCompaniaTopX(actual.nombre(), numServicios));
					}
					actual = s.compania();
					numServicios = 1;
				}
				else
				{
					numServicios++;
				}
			}
		}

		infoCompanias.sort(COMPARATOR_INFO_COMPANIA_TOP_X);
		List<InfoCompaniaTopX> companias = new List<InfoCompaniaTopX>();
		Iterator<InfoCompaniaTopX> iterInfoCompania = infoCompanias.iterator();
		int i = 0;

		while(i < n && iterInfoCompania.hasNext())
		{
			i++;
			companias.agregarAlFinal(iterInfoCompania.next());
		}

		return companias;
	}

	public List<Taxi> c3()
	{
		return taxisMasRentables;
	}

	public IStack<Servicio> c4(String id, long inicio, long fin) throws Exception
	{
		IStack<Servicio> servicios = new Stack<Servicio>();
		Iterator<Taxi> iterTaxis = taxis.iterator();
		Taxi t = null;
		while(iterTaxis.hasNext()) 
		{
			t = iterTaxis.next();
			if(t.id().equals(id))
			{
				break;
			}
		}
		if(t == null)
		{
			throw new Exception("No existe un taxi con tal Id.");
		}

		Servicio s = null;
		double cuenta = 0, gananciaTotal = 0;
		long inicioMenor = Long.MAX_VALUE, finMayor = 0, duracionTotal = 0;
		List<Servicio> cuentirri = new List<Servicio>();
		Iterator<Servicio> iterServicios = t.serviciosPorFecha().iterator();
		while(iterServicios.hasNext())
		{
			s = iterServicios.next();
			if(s.inicio() >= inicio && s.fin() <= fin)
			{
				if(cuenta >= 10)
				{
					servicios.push(new Servicio(null, inicioMenor, finMayor, gananciaTotal,
							cuenta, duracionTotal, null, null, t, t.compania()));
					cuentirri = new List<Servicio>();
					cuenta = 0;
				}
				else
				{
					cuenta += s.distancia();
					gananciaTotal += s.ganancia();
					inicioMenor = s.inicio() <= inicioMenor ? s.inicio() : inicioMenor;
					finMayor = s.fin() >= finMayor ? s.fin() : finMayor; 
					duracionTotal += s.tiempo();
					cuentirri.agregarAlFinal(s);
				}
			}
			else if(s.fin() > fin)
			{
				break;
			}
		}
		if(s == null)
		{
			throw new Exception("El taxi no tiene servicios.");
		}
		Iterator<Servicio> iterCuentirri = cuentirri.iterator();
		while(iterCuentirri.hasNext())
		{
			servicios.push(iterCuentirri.next());
		}

		return servicios;
	}
}
