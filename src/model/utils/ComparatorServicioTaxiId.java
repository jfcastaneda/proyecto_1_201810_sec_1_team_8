package model.utils;

import java.util.Comparator;

import model.vo.Servicio;

public class ComparatorServicioTaxiId implements Comparator<Servicio>
{
	public int compare(Servicio s1, Servicio s2)
	{
		return s1.taxi().id().compareTo(s2.taxi().id());
	}
}
