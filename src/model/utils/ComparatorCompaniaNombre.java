package model.utils;

import java.util.Comparator;

import model.vo.Compania;

public class ComparatorCompaniaNombre implements Comparator<Compania>
{
	public int compare(Compania c1, Compania c2)
	{
		return c1.nombre().compareTo(c2.nombre());
	}
}
