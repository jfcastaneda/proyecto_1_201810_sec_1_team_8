package model.utils;

import java.util.Comparator;

import model.vo.Servicio;

public class ComparatorServicioCompaniaNombre implements Comparator<Servicio>
{
	public int compare(Servicio s1, Servicio s2)
	{
		return s1.compania().nombre().compareTo(s2.compania().nombre());
	}
}
