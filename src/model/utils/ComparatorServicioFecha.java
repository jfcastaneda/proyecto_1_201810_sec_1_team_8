package model.utils;

import java.util.Comparator;

import model.vo.Servicio;

public class ComparatorServicioFecha implements Comparator<Servicio>
{
	public int compare(Servicio s1, Servicio s2)
	{
		if(s1.inicio() <= s2.inicio())
		{ 
			return -1;
		}
		else if(s1.inicio() == s2.inicio())
		{
			if(s1.fin() <= s2.fin())
			{
				return -1;
			}
			else if(s1.fin() == s2.fin())
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 1;
		}
	}
}
