package model.utils;

import java.util.Comparator;

import model.vo.InfoCompaniaTopX;

public class ComparatorInfoCompaniaTopX implements Comparator<InfoCompaniaTopX>
{
	public int compare(InfoCompaniaTopX c1, InfoCompaniaTopX c2)
	{
		if(c1.cantidadServicios() <= c2.cantidadServicios())
		{
			return -1;
		}
		else if(c1.cantidadServicios() == c2.cantidadServicios())
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}
