package model.vo;

public class InfoZonaB4 
{
	private String id;
	
	private int cantidadIniciados;
	
	public InfoZonaB4(String id, int cantidadIniciados)
	{
		this.id = id;
		this.cantidadIniciados = cantidadIniciados;
	}
	
	public String id()
	{
		return id;
	}
	
	public int cantidadIniciados()
	{
		return cantidadIniciados;
	}
}
