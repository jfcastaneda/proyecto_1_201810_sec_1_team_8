package model.vo;

public class InfoCompaniaTopX 
{
	private String nombre;
	
	private int cantidadServicios;
	
	public InfoCompaniaTopX(String nombre, int cantidadServicios)
	{
		this.nombre = nombre;
		this.cantidadServicios = cantidadServicios;
	}
	
	public String nombre()
	{
		return nombre;
	}
	
	public int cantidadServicios()
	{
		return cantidadServicios;
	}
}
