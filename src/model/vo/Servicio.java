package model.vo;

public class Servicio
{
	private String id;
	
	private long inicio;
	
	private long fin;
	
	private double ganancia;
	
	private double distancia;
	
	private long tiempo;
	
	private Zona salida;
	
	private Zona llegada;
	
	private Taxi taxi;
	
	private Compania compania;
	
	public Servicio(String id, long inicio, long fin, double ganancia, double distancia,
			long tiempo, Zona salida, Zona llegada, Taxi taxi, Compania compania)
	{
		this.id = id;
		this.inicio = inicio;
		this.fin = fin; 
		this.ganancia = ganancia;
		this.distancia = distancia;
		this.tiempo = tiempo;
		this.salida = salida;
		this.llegada = llegada;
		this.taxi = taxi;
		this.compania = compania;
	}
	
	public String id()
	{
		return id;
	}
	
	public long inicio()
	{
		return inicio;
	}
	
	public long fin()
	{
		return fin;
	}
	
	public double ganancia()
	{
		return ganancia;
	}
	
	public double distancia()
	{
		return distancia;
	}
	
	public long tiempo()
	{
		return tiempo;
	}
	
	public Zona salida()
	{
		return salida;
	}
	
	public Zona llegada()
	{
		return llegada;
	}
	
	public Taxi taxi()
	{
		return taxi;
	}
	
	public Compania compania()
	{
		return compania;
	}
	
	public void cambiarSalida(Zona salida)
	{
		this.salida = salida;
	}
	
	public void cambiarLlegada(Zona llegada)
	{
		this.llegada = llegada;
	}
	
	public String toString()
	{
		return id;
	}
}
