package model.data_structures;

public class Stack<E> implements IStack<E> 
{
	private List<E> list;
	
	/**
	 * Constructor.
	 */
	public Stack()
	{
		list = new List<E>();
	}
	
	/** Push a new element at the top of the stack */
	public void push (E item)
	{
		list.agregarAlPrincipio(item);
	}
	
	/** Pop the element at the top of the stack 
	 * @return the top element or null if it doesn't exist
	 * */
	public E pop()
	{
		return list.quitarPrimero();
	}
	
	/** Evaluate if the stack is empty
	 * @return true if the stack is empty. false in other case. 
	 */
	public boolean isEmpty()
	{
		return list.isEmpty();
	}
}
